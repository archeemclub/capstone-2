//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const productRoutes = require('./routes/product');
	const userRoutes = require('./routes/user');
	
//[SECTION] Environment Variables Setup
	dotenv.config();
	const port = process.env.PORT;
	const credentials = process.env.MONGO_URL;

//[SECTION] Server Setup
	const app = express();
	app.use(express.json());

//[SECTION] Database Connect
	mongoose.connect(credentials);
	const db = mongoose.connection; 
	db.once('open', () => console.log(`Connected to Capstone`)); 

//[SECTION] Server Routes
	app.use('/product', productRoutes);
	app.use('/user', userRoutes); 

//[SECTION] Server Responses
	app.get('/', (req, res) => {
		res.send(`Successfully Deployed`);
	});
	app.listen(port, () => {
   		console.log(`Capstone Port is now Online! ${port}`);
	});
